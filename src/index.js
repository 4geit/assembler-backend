const { GraphQLServer } = require('graphql-yoga')
const { Prisma } = require('prisma-binding')
const dotenv = require('dotenv')

// load .env file variables and make them available under process.env
dotenv.config()

const resolvers = {
  Query: {
    occurrences(parent, { sessionId, includeChildren }, ctx, info) {
      let fields = {}
      if (!includeChildren) {
        fields.parent = null
      }
      return ctx.db.query.occurrences({
        where: {
          session: { id: sessionId },
          ...fields
        }
      }, info)
    },
    occurrence(parent, { id }, ctx, info) {
      return ctx.db.query.occurrence({ where: { id } }, info)
    },
    session(parent, { id }, ctx, info) {
      return ctx.db.query.session({ where: { id } }, info)
    },
  },
  Mutation: {
    createSession(parent, args, ctx, info) {
      return ctx.db.mutation.createSession({ data: {} }, info)
    },
    updateSession(parent, { id, leftOpen, rightOpen }, ctx, info) {
      return ctx.db.mutation.updateSession({
        where: { id },
        data: {
          leftOpen,
          rightOpen,
        },
      })
    },
    focusOccurrence(parent, { sessionId, occurrenceId }, ctx, info) {
      return ctx.db.mutation.updateSession({
        where: { id: sessionId },
        data: {
          focusedOccurrence: {
            connect: {
              id: occurrenceId,
            },
          },
        },
      }, info)
    },
    unfocusOccurrence(parent, { sessionId, occurrenceId }, ctx, info) {
      return ctx.db.mutation.updateSession({
        where: { id: sessionId },
        data: {
          focusedOccurrence: {
            disconnect: {
              id: occurrenceId,
            },
          },
        },
      }, info)
    },
    focusProp(parent, { occurrenceId, propId }, ctx, info) {
      return ctx.db.mutation.updateOccurrence({
        where: { id: occurrenceId },
        data: {
          focusedProp: {
            connect: {
              id: propId,
            },
          },
        },
      }, info)
    },
    unfocusProp(parent, { occurrenceId, propId }, ctx, info) {
      return ctx.db.mutation.updateOccurrence({
        where: { id: occurrenceId },
        data: {
          focusedProp: {
            disconnect: {
              id: propId,
            },
          },
        },
      }, info)
    },
    updateOccurrence(parent, { id, isChecked }, ctx, info) {
      return ctx.db.mutation.updateOccurrence({
        where: { id },
        data: {
          isChecked,
        },
      }, info)
    },
    createOccurrence(parent, { sessionId, component, parentId, props }, ctx, info) {
      const fields = {}
      if (parentId) {
        fields.parent = {
          connect: {
            id: parentId,
          },
        }
      }
      if (props) {
        fields.props = {
          create: props,
        }
      }
      return ctx.db.mutation.createOccurrence({
        data: {
          session: {
            connect: {
              id: sessionId,
            },
          },
          component,
          ...fields,
        },
      }, info)
    },
    async deleteCheckedOccurrences(parent, { sessionId }, ctx, info) {
      // FIXME: temporaly solutions to remove nested node because
      // casade delete not supported with `deleteManyX`
      // (see https://github.com/graphcool/prisma/issues/1936)
      await ctx.db.mutation.deleteManyProps({
        where: {
          occurrence: {
            // FIXME: this throws a server error
            // session: {
            //   id: sessionId,
            // },
            isChecked: true,
          },
        },
      })
      // remove all the occurrences that are checked and session matches
      await ctx.db.mutation.deleteManyOccurrences({
        where: {
          session: {
            id: sessionId,
          },
          isChecked: true,
        },
      })
      // return the new list of occurrences
      return ctx.db.query.occurrences({
        where: {
          session: {
            id: sessionId,
          },
        },
      }, info)
    },
    updateProp(parent, { id, value }, ctx, info) {
      return ctx.db.mutation.updateProp({
        where: {
          id,
        },
        data: {
          value,
        },
      }, info)
    },
  },
  Subscription: {
    session: {
      subscribe(parent, args, ctx, info) {
        return ctx.db.subscription.session({ where: {} }, info)
      },
    },
    occurrence: {
      subscribe(parent, { sessionId }, ctx, info) {
        return ctx.db.subscription.occurrence({
          where: {
            node: {
              session: {
                id: sessionId,
              },
            },
          },
        }, info)
      },
    },
    prop: {
      subscribe(parent, { sessionId }, ctx, info) {
        return ctx.db.subscription.prop({
          where: {
            node: {
              occurrence: {
                session: {
                  id: sessionId,
                },
              },
            },
          },
        }, info)
      },
    },
  },
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: process.env.PRISMA_ENDPOINT, // the endpoint of the Prisma DB service
      secret: process.env.PRISMA_SECRET, // specified in database/prisma.yml
      debug: false, // log all GraphQL queryies & mutations
    }),
  }),
})

server.start(() => console.log('Server is running on http://localhost:4000'))
