#!/bin/sh
echo "generate .prisma folder"
mkdir -p ~/.prisma
echo "create config.yml file"
cat << EOF > ~/.prisma/config.yml
clusters:
  $PRISMA_CLUSTER:
    host: '$PRISMA_CLUSTER_HOST'
    clusterSecret: "$PRISMA_CLUSTER_SECRET_KEY"
EOF
