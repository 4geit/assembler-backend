# Assembler Backend

---

GraphQL backend for the Assembler tool

## Installation

To get started, you will need first to clone the repository thanks to the command line:

```bash
git clone git@gitlab.com:4geit/assembler-backend.git
```

or through the HTTPS URL:

```bash
git clone https://gitlab.com/4geit/assembler-backend.git
```

then you can `cd` to the new create folder as follow:

```bash
cd assembler-backend
```

and install the dependencies with `yarn`

```bash
yarn
```

You will also need to setup a set of environment variables by creating a new `.env` file at the root of the repository and add those environement variables as follow:

```
PRISMA_STAGE = __PRISMA_STAGE__
PRISMA_CLUSTER = __PRISMA_CLUSTER__
PRISMA_SECRET = __PRISMA_SECRET__
PRISMA_ENDPOINT = __PRISMA_ENDPOINT__
MAILGUN_API_KEY = __MAILGUN_API_KEY__
MAILGUN_DOMAIN = __MAILGUN_DOMAIN__
```

You can now run a local instance of the GraphQL server using the command:

```bash
yarn start
```

You will also need to create a Prisma instance of database either by setting a local docker container or by deploying it on the Prisma cloud with the command:

```bash
yarn prisma deploy
```

## Deployment

The GraphQL server will be automatically deployed, into a staging environment of the remote prisma cluster, everytime you push/merge your changes into the branch `master`. The deploy will also occur in a different environment (production) whenever you merge into the branch `production`. See the file [.gitlab-ci.yml](./.gitlab-ci.yml) for more details.
